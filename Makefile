#!/usr/bin/make -f
# Makefile for jack2-simple-config #
# ---------------------- # ------------------------- #
# This generates python code from QtDesigner UI files #
# ---------------------------------------------------- #
# Created by falkTX
#

PREFIX = /usr
DESTDIR =

all:build

build:
	pyuic4 -o ./src/ui_mainw.py ./ui/mainw.ui
	pyuic4 -o ./src/ui_wait.py ./ui/wait.ui
	pyrcc4 -o ./src/icons_rc.py ./ui/icons.qrc
	# Jack Settings port from Cadence:
	pyuic4 -o ./src/ui_settings_jack.py ./jacksettings/settings_jack.ui
	cp jacksettings/*.py src/

distclean:clean

clean:
	rm -f *~ src/*~ src/*.pyc src/jacksettings.py src/ui_*.py src/icons_rc.py

install:
	# Make directories
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -d $(DESTDIR)$(PREFIX)/share/applications/
	install -d $(DESTDIR)$(PREFIX)/share/pixmaps/
	install -d $(DESTDIR)$(PREFIX)/share/jack2-simple-config/
	# Install files
	install -m 655 jack2-simple-config $(DESTDIR)$(PREFIX)/bin/
	install -m 644 jack2-simple-config.desktop $(DESTDIR)$(PREFIX)/share/applications/
	install -m 644 jack2-simple-config.svg $(DESTDIR)$(PREFIX)/share/pixmaps/
	install -m 655 src/*.py $(DESTDIR)$(PREFIX)/share/jack2-simple-config/
