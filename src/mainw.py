#!/usr/bin/env python
# -*- coding: utf-8 -*-


#Imports
import os, sys, alsabridge, jacksettings, icons_rc, ui_mainw, ui_wait
from commands import getoutput
from dbus import UInt32, String
from functools import partial
from time import sleep
from PyQt4.QtCore import Qt, QFile, QIODevice, QPropertyAnimation, QSettings, QSize, QString, QTextStream, QThread, QTimer, QVariant, SIGNAL
from PyQt4.QtGui import QDialog, QFileDialog, QIcon, QMainWindow, QMenu, QMessageBox, QSystemTrayIcon
from xicon import XIcon


HOME = os.getenv("HOME")
if (not os.path.exists(HOME+"/.kxstudio")):
  os.mkdir(HOME+"/.kxstudio")


# DBus connections
class DBus(object):
    __slots__ = [
            'loopBus',
            'jackBus',
            'controlBus',
            'studioBus',
    ]
DBus = DBus()



# Main Window
class ForceWaitDialog(QDialog, ui_wait.Ui_Dialog):
    def __init__(self, parent=None):
        super(ForceWaitDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(Qt.Dialog|Qt.WindowCloseButtonHint)

# Wait while JACK restarts
class ForceRestartThread(QThread):
    def __init__(self):
        QThread.__init__(self)

        self._ladish = None
        self._studio_name = None
        self._pulse = None
        self._pulse_play = None
        self._started = None

    def setLoadState(self, ladish, studio_name, pulse, pulse_play):
        self._ladish = ladish
        self._studio_name = studio_name
        self._pulse = pulse
        self._pulse_play = pulse_play
        self._started = None

    def wasJackStarted(self):
        return self._started

    def run(self):
        # Not started yet
        self._started = False

        # Kill All
        proc_term = "a2j a2jmidid jackd jackdmp lashd ladishd ladiconfd jmcore"
        proc_kill = "jackdbus pulseaudio"

        os.system("killall " + proc_term)
        remaining_proc = True
        while remaining_proc:
          print "checking terminated"
          remaining_proc = False
          sleep(0.1)
          for i1 in getoutput('ps -U $USER -o comm=').split():
            if remaining_proc: break
            for i2 in proc_term.split():
              if i2 == i1:
                remaining_proc = True
                break

        os.system("killall -KILL " + proc_kill)
        remaining_proc = True
        while remaining_proc:
          print "checking terminated"
          remaining_proc = False
          sleep(0.1)
          for i1 in getoutput('ps -U $USER -o comm=').split():
            if remaining_proc: break
            for i2 in proc_kill.split():
              if i2 == i1:
                remaining_proc = True
                break

        print "Ignore all previous \"no process found\" messages\n"

        # 1) Re-connect DBus and... 2) get necessary objects, causing appropriate daemons to start
        DBus.jackBus = DBus.loopBus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        DBus.controlBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Control")
        DBus.studioBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")

        # Be sure, that jackdbus and ladishd really started
        while os.system('ps -U $USER -o comm= | grep -x jackdbus') != 0:
          sleep(0.1)

        while os.system('ps -U $USER -o comm= | grep -x ladishd') != 0:
          sleep(0.1)

        # Start JACK
        if (self._ladish):
          DBus.controlBus.LoadStudio(String(self._studio_name))
          DBus.studioBus.Start()
        else:
          if bool(DBus.controlBus.IsStudioLoaded()):
            DBus.studioBus.Start()
          else:
            DBus.jackBus.StartServer()

        if (self._pulse):
          if (self._pulse_play):
            os.system("pulse-jack -p")
          else:
            os.system("pulse-jack")

        # If we made it this far, then JACK is started
        self._started = True

# Main Window
class MainW(QMainWindow, ui_mainw.Ui_MainW):
    def __init__(self, *args):
        QMainWindow.__init__(self, *args)
        self.setupUi(self)

        self.loadSettings()

        DBus.jackBus = DBus.loopBus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")

        # Set this before the connections, or a getOpenFileName may be called
        if (os.path.exists(HOME+"/.asoundrc")):
          self.ch_alsa.setChecked(True)
          AlsaConf = open(HOME+"/.asoundrc", "r").read()
          if ("slave { pcm \"jack\" }" in AlsaConf):
            self.co_alsa.setCurrentIndex(0)
          elif ("slave { pcm \"pulse\" }" in AlsaConf) or ("slave { pcm \"pulseaudio\" }" in AlsaConf):
            self.co_alsa.setCurrentIndex(1)
          else:
            self.co_alsa.setCurrentIndex(2)

        # Internal timer
        self.timer = QTimer()
        self.timer.start(2000)

        # Variables
        self.stopped_manually = False
        self.studio_name = QString("")

        # Sey Buffer Size as checkboxes
        self.act_bf_list = ( self.act_bf_32, self.act_bf_64, self.act_bf_128, self.act_bf_256, self.act_bf_512,
                             self.act_bf_1024, self.act_bf_2048, self.act_bf_4096, self.act_bf_8192 )

        if bool(DBus.jackBus.IsStarted()):
          try:
            size = str(DBus.jackBus.GetBufferSize())
            self.menuBufferSize(size)
          except:
            self.menuBufferSize(0)
        else:
          self.menuBufferSize(0)

        # Set Icons
        self.MyIcons = XIcon()
        self.MyIcons.addThemeName(str(unicode(QIcon.themeName()).encode('utf-8')))
        self.MyIcons.addThemeName("oxygen") #Just in case...

        self.act_close.setIcon(QIcon(self.MyIcons.getIconPath("application-exit", 16)))
        self.act_start.setIcon(QIcon(self.MyIcons.getIconPath("media-playback-start", 16)))
        self.act_stop.setIcon(QIcon(self.MyIcons.getIconPath("media-playback-stop", 16)))
        self.act_force.setIcon(QIcon(self.MyIcons.getIconPath("media-seek-forward", 16)))
        self.act_config.setIcon(QIcon(self.MyIcons.getIconPath("configure", 16)))
        self.act_x_clear.setIcon(QIcon(self.MyIcons.getIconPath("edit-clear", 16)))
        self.act_x_logs.setIcon(QIcon(self.MyIcons.getIconPath("ladilog", 16)))
        self.act_x_refresh.setIcon(QIcon(self.MyIcons.getIconPath("view-refresh", 16)))
        self.act_about.setIcon(QIcon(":/jack2-simple-config.svg"))

        self.b_start.setIcon(QIcon(self.MyIcons.getIconPath("media-playback-start", 16)))
        self.b_stop.setIcon(QIcon(self.MyIcons.getIconPath("media-playback-stop", 16)))
        self.b_force.setIcon(QIcon(self.MyIcons.getIconPath("media-seek-forward", 16)))
        self.b_config.setIcon(QIcon(self.MyIcons.getIconPath("configure", 16)))

        self.icon_apply = self.MyIcons.getIconPath("dialog-ok-apply", 16)
        self.icon_cancel = self.MyIcons.getIconPath("dialog-cancel", 16)

        # Systray
        self.trayIconMenu = QMenu(self)
        self.trayIconMenu.addAction(self.act_start)
        self.trayIconMenu.addAction(self.act_stop)
        self.trayIconMenu.addAction(self.act_force)
        self.trayIconMenu.addAction(self.act_config)
        self.trayIconMenu.addSeparator()

        self.menuBF = QMenu(self.tr("Set &Buffer Size"), self)
        self.menuBF.addAction(self.act_bf_32)
        self.menuBF.addAction(self.act_bf_64)
        self.menuBF.addAction(self.act_bf_128)
        self.menuBF.addAction(self.act_bf_256)
        self.menuBF.addAction(self.act_bf_512)
        self.menuBF.addAction(self.act_bf_1024)
        self.menuBF.addAction(self.act_bf_2048)
        self.menuBF.addAction(self.act_bf_4096)
        self.menuBF.addAction(self.act_bf_8192)
        self.trayIconMenu.addMenu(self.menuBF)

        self.trayIconMenu.addAction(self.act_x_logs)
        self.trayIconMenu.addAction(self.act_x_clear)
        self.trayIconMenu.addAction(self.act_x_refresh)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.act_close)

        self.trayIcon = QSystemTrayIcon(self)
        self.trayIcon.setContextMenu(self.trayIconMenu)
        self.trayIcon.setIcon(QIcon(":/jack2-simple-config.svg"))
        self.trayIcon.activated.connect(self.func_Systray_Clicked)
        self.trayIcon.show()

        # Connect actions to functions
        self.connect(self.act_close, SIGNAL("triggered()"), self.close)
        self.connect(self.act_start, SIGNAL("triggered()"), self.func_Start)
        self.connect(self.act_stop, SIGNAL("triggered()"), self.func_Stop)
        self.connect(self.act_force, SIGNAL("triggered()"), self.func_Force)
        self.connect(self.act_config, SIGNAL("triggered()"), self.func_Config)
        self.connect(self.b_start, SIGNAL("clicked()"), self.func_Start)
        self.connect(self.b_stop, SIGNAL("clicked()"), self.func_Stop)
        self.connect(self.b_force, SIGNAL("clicked()"), self.func_Force)
        self.connect(self.b_config, SIGNAL("clicked()"), self.func_Config)
        self.connect(self.act_x_clear, SIGNAL("triggered()"), self.func_ClearXrun)
        self.connect(self.act_x_logs, SIGNAL("triggered()"), self.func_Logs)
        self.connect(self.act_x_refresh, SIGNAL("triggered()"), self.refreshJackStatus)
        self.connect(self.act_bf_32, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 32))
        self.connect(self.act_bf_64, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 64))
        self.connect(self.act_bf_128, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 128))
        self.connect(self.act_bf_256, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 256))
        self.connect(self.act_bf_512, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 512))
        self.connect(self.act_bf_1024, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 1024))
        self.connect(self.act_bf_2048, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 2048))
        self.connect(self.act_bf_4096, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 4096))
        self.connect(self.act_bf_8192, SIGNAL("triggered()"), partial(self.func_SetBufferSize, 8192))
        self.connect(self.act_about, SIGNAL("triggered()"), self.func_About)
        self.connect(self.timer, SIGNAL("timeout()"), self.refreshJackStatus)
        self.connect(self.ch_pulsejack, SIGNAL("toggled(bool)"), self.func_clickedPulse)
        self.connect(self.ch_pulse_playback_only, SIGNAL("toggled(bool)"), self.func_clickedPulsePlayback)
        self.connect(self.ch_alsa, SIGNAL("toggled(bool)"), self.func_clickedAlsa)
        self.connect(self.co_alsa, SIGNAL("currentIndexChanged(QString)"), self.func_changedAlsa)
        self.connect(self.ch_ladish, SIGNAL("toggled(bool)"), self.func_clickedLadish)

        # Create D-Bus signals handlers
        DBus.loopBus.add_signal_receiver(self.updateLadishDBus, sender_keyword='sender', destination_keyword='dest', interface_keyword='interface',
                        member_keyword='member', path_keyword='path')

        self.l_osname.setText(os.uname()[0])
        self.l_arch.setText(os.uname()[4])
        self.l_kernel.setText(os.uname()[2])
        self.l_realtime.setText("")

        if (os.path.exists("/usr/bin/pulse-jack") or os.path.exists("/usr/local/bin/pulse-jack") or os.path.exists(HOME+"/bin/pulse-jack")):
          if (not os.path.exists(HOME+"/.kxstudio/no-pulse")):
            self.ch_pulsejack.setChecked(True)
            if (os.path.exists(HOME+"/.kxstudio/pulse-playback-only")):
              self.ch_pulse_playback_only.setChecked(True)
        else:
          self.ch_pulsejack.setEnabled(False)
          self.ch_pulse_playback_only.setEnabled(False)

        self.refreshDBus()
        self.refreshJackStatus()
        self.refreshState()
        self.refreshLadish()

        # Fix auto-reset startup studio
        self.connect(self.co_ladish, SIGNAL("currentIndexChanged(QString)"), self.func_changedLadish)

    def refreshDBus(self):
        DBus.jackBus = DBus.loopBus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        DBus.controlBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Control")
        DBus.studioBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")

    def refreshJackStatus(self):
        if bool(DBus.jackBus.IsStarted()):
          latency = DBus.jackBus.GetLatency()
          if (latency < 10):
            latD = 3
          elif (latency < 100):
            latD = 4
          elif (latency < 1000):
            latD = 5
          elif (latency < 10000):
            latD = 6
          str_latency = str(latency)[0:latD]

          self.l_jackserver.setText(self.tr("Running"))
          self.l_ico_jackserver.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-ok-apply", 16)).pixmap(16, 16))
          self.l_samplerate.setText(str(DBus.jackBus.GetSampleRate())+" Hz")
          self.l_latency.setText(str_latency+" ms")
          self.l_buffersize.setText(str(DBus.jackBus.GetBufferSize())+" Samples")
          self.l_xruns.setText(str(DBus.jackBus.GetXruns()))

          cpuload = str(os.getloadavg()[0])[0:4]
          self.l_cpuload.setText(str(cpuload)+"%")

          dspload = str(DBus.jackBus.GetLoad())[0:4]
          self.l_dspload.setText(str(dspload)+"%")

          if bool(DBus.jackBus.IsRealtime()):
            self.l_realtime.setText(self.tr("Yes"))
            self.l_ico_realtime.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-ok-apply", 16)).pixmap(16, 16))
          else:
            self.l_realtime.setText(self.tr("No"))
            self.l_ico_realtime.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-cancel", 16)).pixmap(16, 16))

          try:
            size = str(DBus.jackBus.GetBufferSize())
            self.menuBufferSize(size)
          except:
            self.menuBufferSize(0)

          self.act_force.setIcon(QIcon(self.MyIcons.getIconPath("dialog-warning", 16)))
          self.b_force.setIcon(QIcon(self.MyIcons.getIconPath("dialog-warning", 16)))

          ico_jackserver = self.icon_apply

        else:
          self.l_jackserver.setText(self.tr("Stopped"))
          self.l_ico_jackserver.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-cancel", 16)).pixmap(16, 16))
          self.l_samplerate.setText("---")
          self.l_latency.setText("---")
          self.l_buffersize.setText("---")
          self.l_xruns.setText("---")
          self.l_dspload.setText("---")
          self.menuBufferSize(0)
          self.act_force.setIcon(QIcon(self.MyIcons.getIconPath("media-seek-forward", 16)))
          self.b_force.setIcon(QIcon(self.MyIcons.getIconPath("media-seek-forward", 16)))
          ico_jackserver = self.icon_cancel

          cpuload = str(os.getloadavg()[0])[0:4]
          self.l_cpuload.setText(str(cpuload)+"%")

        systrayText = self.tr("<table>"
                       "<tr><td align='center' colspan='2'><h4>Jack2 Simple Config</h4></td></tr>"
                       "<tr><td align='right'>Status:</td><td>%1</td></tr>"
                       "<tr><td align='right'>Realtime:</td><td>%2</td></tr>"
                       "<tr><td align='right'>DSP Load:</td><td>%3</td></tr>"
                       "<tr><td align='right'>Xruns:</td><td>%4</td></tr>"
                       "<tr><td align='right'>Sample Rate:</td><td>%5</td></tr>"
                       "<tr><td align='right'>Latency:</td><td>%6</td></tr>"
                       "<tr><td align='right'>Buffer Size:</td><td>%7</td></tr>"
                       "</table>").arg(self.l_jackserver.text()).arg(
                       self.l_realtime.text()).arg(self.l_dspload.text()).arg(self.l_xruns.text()).arg(
                       self.l_samplerate.text()).arg(self.l_latency.text()).arg(self.l_buffersize.text())
        self.trayIcon.setToolTip(systrayText)

        self.refreshState()

    def refreshState(self):
       if bool(DBus.jackBus.IsStarted()):
         self.b_start.setEnabled(False)
         self.b_stop.setEnabled(True)
         self.b_config.setEnabled(True)
         self.act_start.setEnabled(False)
         self.act_stop.setEnabled(True)
         self.act_config.setEnabled(True)
       else:
         self.b_start.setEnabled(True)
         self.b_stop.setEnabled(False)
         self.b_config.setEnabled(True)
         self.act_start.setEnabled(True)
         self.act_stop.setEnabled(False)
         self.act_config.setEnabled(True)

    def refreshLadish(self):
        ladish_studio_list = []
        lalist = DBus.controlBus.GetStudioList()
        for i in range(len(lalist)):
          ladish_studio_list.append(unicode(lalist[i][0]))

        current_studio_list = []
        for i in range(self.co_ladish.count()):
          current_studio_list.append(self.co_ladish.itemText(i))

        saveFileName = HOME+"/.kxstudio/default-studio-name"
        if os.path.exists(saveFileName):
          studio_file = QFile(saveFileName)
          studio_file.open(QIODevice.ReadOnly)
          stream = QTextStream(studio_file)
          stream.setCodec("UTF-8")
          self.studio_name = stream.readAll().split("\n")[0]
          self.ch_ladish.setChecked(True)

        # Update studio list
        for i in range(len(current_studio_list)):
          if not current_studio_list[i] in ladish_studio_list:
            self.co_ladish.removeItem(i)

        for i in range(len(ladish_studio_list)):
          if not ladish_studio_list[i] in current_studio_list:
            self.co_ladish.addItem(ladish_studio_list[i])

        # Set studio
        if (self.ch_ladish.isChecked()):
          for i in range(self.co_ladish.count()):
            if (self.co_ladish.itemText(i) == self.studio_name):
              self.co_ladish.setCurrentIndex(i)
              break

        self.func_changedLadish(self.studio_name)

    def func_Start(self):
        try:
          if bool(DBus.controlBus.IsStudioLoaded()):
            DBus.studioBus.Start()
          else:
            DBus.jackBus.StartServer()
          if (self.ch_pulsejack.isChecked()):
            sleep(0.5)
            if (self.ch_pulse_playback_only.isChecked()):
              os.system("pulse-jack -p")
            else:
              os.system("pulse-jack")
        except:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not start Jack!"))
        self.refreshJackStatus()

    def func_Stop(self):
        self.stopped_manually = True
        #self.timer.stop()
        try:
          if bool(DBus.controlBus.IsStudioLoaded()):
            DBus.studioBus.Stop()
          else:
            DBus.jackBus.StopServer()
        except:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not stop Jack!"))
        #self.timer.start(2000)

    def func_Force(self):
        if bool(DBus.jackBus.IsStarted()):
          ask_r = self.customMessageBox(QMessageBox.Warning, self.tr("Warning"),
                  self.tr("This will force kill all Jack daemons!<br>Make sure to save your projects before continuing."),
                  self.tr("Are you sure you want to force the restart of Jack?"))
          if (ask_r != QMessageBox.Yes):
            return

        if (self.ch_ladish.isChecked()):
          ask = self.customMessageBox(QMessageBox.Question, self.tr("Load LADI Studio Now?"),
                  self.tr("You have set a LADI Studio to auto-load on session startup.<br>Do you want to load the Studio '<b>%1</b>' now?").arg(self.studio_name))
        else:
          ask = QMessageBox.No

        self.timer.stop()

        self.wait_dialog = ForceWaitDialog(self)
        self.animation = QPropertyAnimation(self.wait_dialog.progressBar, "value")
        self.animation.setDuration(7000)
        self.animation.setStartValue(0)
        self.animation.setEndValue(100)
        self.animation.start()

        self.fr_thread = ForceRestartThread()
        self.fr_thread.setLoadState(ask == QMessageBox.Yes, self.studio_name, self.ch_pulsejack.isChecked(), self.ch_pulse_playback_only.isChecked())
        self.fr_thread.start()
        self.connect(self.fr_thread, SIGNAL("finished()"), self.func_ForceFinished)
        self.wait_dialog.exec_()

    def func_ForceFinished(self):
        self.wait_dialog.close()

        if (not self.sender().wasJackStarted()):
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not start Jack!"))

        # Get ladish Busses again (seems to only work when jackdbus has been activated/started at least once)
        self.refreshDBus()
        self.refreshJackStatus()
        self.timer.start(2000)

    def func_Config(self):
        jacksettings.initBus(DBus.loopBus)
        jacksettings.JackSettingsW().exec_()
        self.refreshState()

    def func_ClearXrun(self):
        DBus.jackBus.ResetXruns()
        self.refreshJackStatus()

    def func_Logs(self):
        os.system("ladilog &") #TODO - port logs code from Cadence, when ready

    def func_About(self):
        QMessageBox.about(self, self.tr("About Jack2 Simple Config"), self.tr("<h3>Jack2 Simple Config</h3>"
            "<br>Version 0.4.0"
            "<br>Jack2 Simple Config ('j2sc' for short) is a small utility that makes configuring JACK2 a very simple thing.<br>"
            "<br>Copyright (C) 2010 falkTX"))

    def func_Systray_Clicked(self, reason):
        if (reason == 2 or reason == 3 or reason == 4):
          if self.isHidden():
            self.show()
          else:
            self.hide()

    def func_SetBufferSize(self, size):
        if bool(DBus.jackBus.IsStarted()):
          try:
            DBus.jackBus.SetBufferSize(UInt32(size))
          except:
            QMessageBox.critical(self, self.tr("Error"), self.tr("Could not set buffer size!"))
            size = str(DBus.jackBus.GetBufferSize())
          self.menuBufferSize(size)
        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Cannot set buffer size while Jack is not running"))
          self.menuBufferSize(0)

        self.refreshJackStatus()

    def menuBufferSize(self, buffer_size):
        if (buffer_size):
          for i in range(len(self.act_bf_list)):
            self.act_bf_list[i].setEnabled(True)
            if (self.act_bf_list[i].text().replace("&","") == str(buffer_size)):
              if (not self.act_bf_list[i].isChecked()):
                self.act_bf_list[i].setChecked(True)
            else:
              if (self.act_bf_list[i].isChecked()):
                self.act_bf_list[i].setChecked(False)
        else:
          for i in range(len(self.act_bf_list)):
            self.act_bf_list[i].setEnabled(False)

    def func_clickedPulse(self, clicked):
        if (clicked):
          if (os.path.exists(HOME+"/.kxstudio/no-pulse")):
            os.remove(HOME+"/.kxstudio/no-pulse")
        else:
          if (not os.path.exists(HOME+"/.kxstudio/no-pulse")):
            os.mknod(HOME+"/.kxstudio/no-pulse")

    def func_clickedPulsePlayback(self               , clicked):
        if (clicked):
          if (not os.path.exists(HOME+"/.kxstudio/pulse-playback-only")):
            os.mknod(HOME+"/.kxstudio/pulse-playback-only")
        else:
          if (os.path.exists(HOME+"/.kxstudio/pulse-playback-only")):
            os.remove(HOME+"/.kxstudio/pulse-playback-only")

    def func_clickedAlsa(self, clicked):
        if (clicked):
          if (self.co_alsa.currentText() == self.tr("Jack")):
            content = alsabridge.JackFile
          elif (self.co_alsa.currentText() == self.tr("PulseAudio")):
            content = alsabridge.PulseAudioFile
          else:
            print "WARNING: Selected ALSA Bridge config Ignored!"
            return
          fileW = open(HOME+"/.asoundrc", "w")
          fileW.write(content)
          fileW.close()
        else:
          if (os.path.exists(HOME+"/.asoundrc")):
            os.remove(HOME+"/.asoundrc")
          if (self.co_alsa.currentText() == self.tr("Custom...")):
            self.co_alsa.setCurrentIndex(0)

    def func_changedAlsa(self, itemText):
        if (self.ch_alsa.isChecked()):
          if (itemText == self.tr("Jack")):
            content = alsabridge.JackFile
          elif (itemText == self.tr("PulseAudio")):
            content = alsabridge.PulseAudioFile
          elif (itemText == self.tr("Custom...")):
            fileToOpen = QFileDialog.getOpenFileName(self, self.tr("Custom asoundrc file"), "", self.tr("Alsa Sound Configuration Files (*.*)"))
            if (not fileToOpen.isEmpty()):
              content = open(fileToOpen, "r").read()
            else:
              content = alsabridge.JackFile
              self.co_alsa.setCurrentIndex(0)
          else:
            print "WARNING: Selected ALSA Bridge config Ignored! (2)"

          fileW = open(HOME+"/.asoundrc", "w")
          fileW.write(content)
          fileW.close()

    def func_clickedLadish(self, clicked):
        saveFileName = HOME+"/.kxstudio/default-studio-name"
        if (clicked):
          fileW = QFile(saveFileName)
          fileW.open(QIODevice.WriteOnly)
          stream = QTextStream(fileW)
          stream.setCodec("UTF-8")
          stream << self.co_ladish.currentText() << "\n"
        else:
          if (os.path.exists(saveFileName)): os.remove(saveFileName)

    def func_changedLadish(self, itemText):
        if (self.ch_ladish.isChecked()):
          fileW = QFile(HOME+"/.kxstudio/default-studio-name")
          fileW.open(QIODevice.WriteOnly)
          stream = QTextStream(fileW)
          stream.setCodec("UTF-8")
          stream << itemText << "\n"
          self.studio_name = itemText
        else:
          self.studio_name = ""

    def updateLadishDBus(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()
        elif (kwds['interface'] == "org.ladish.Control"):
          if (kwds['member'] == "StudioAppeared" or kwds['member'] == "StudioDisappeared"):
            self.refreshLadish()
        elif (kwds['interface'] == "org.ladish.Studio"):
          if (kwds['member'] == "StudioRenamed"):
            self.refreshLadish()

    def customMessageBox(self, icon, title, text, extra_text=""):
        msgBox = QMessageBox(self)
        msgBox.setIcon(icon)
        msgBox.setWindowTitle(title)
        msgBox.setText(text)
        msgBox.setInformativeText(extra_text)
        msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msgBox.setDefaultButton(QMessageBox.No)
        return msgBox.exec_()

    def jackStarted(self):
        #self.timer.start(2000)
        self.refreshDBus()

    def jackStopped(self):
        #self.timer.stop()
        self.refreshDBus()

        # Check if Jack was stopped somewhere else
        if (not self.stopped_manually):
          pass  # FIXME - GUI hangs in here, why?
          #QMessageBox.information(self, self.tr("Notice"), self.tr(""
            #"Jack has been stopped outside Jack2-Simple-Config.<br>"
            #"Make sure you click on the menu 'Tools'->'Force Refresh' when started again."
            #""))

        self.stopped_manually = False

    def saveSettings(self):
        settings = QSettings()
        settings.setValue("Geometry", QVariant(self.saveGeometry()))

    def loadSettings(self, size_too=True):
        settings = QSettings()
        self.restoreGeometry(settings.value("Geometry").toByteArray())

    def closeEvent(self, event):
        self.saveSettings()
        event.accept()
