#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Imports
import dbus, sys, jacksettings, icons_rc
from dbus.mainloop.qt import DBusQtMainLoop
from PyQt4.QtGui import QApplication, QIcon, QMessageBox
from mainw import MainW, DBus


#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Jack2 Simple Config")
    app.setApplicationVersion("0.4.0")
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/jack2-simple-config.svg"))

    # Connect to DBus
    dbus_loop = DBusQtMainLoop(set_as_default=True)
    DBus.loopBus = dbus.SessionBus(mainloop=dbus_loop)
    if (jacksettings.initBus(DBus.loopBus)):
      QMessageBox.critical(None, app.tr("Error"), app.tr("jackdbus is not available!\nJack2 Simple Config cannot start."))
      sys.exit(-1)

    gui = MainW()
    gui.show()

    sys.exit(app.exec_())
