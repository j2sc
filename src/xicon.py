#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from os import path
from xdg import IconTheme

# TODO - cache icons for faster lookup

class XIcon(object):
    def __init__(self, *args):
      self.extra_themes = []

    def addIconPath(self, path):
      IconTheme.icondirs.append(path)

    def addThemeName(self, theme_name):
      for i in range(len(IconTheme.icondirs)):
        theme_path = path.join(IconTheme.icondirs[i], theme_name)
        if (path.exists(theme_path)):
          self.extra_themes.append(theme_name)

    def getIconPath(self, name, size=48):
      for i in range(len(self.extra_themes)):
        icon = IconTheme.getIconPath(name, size, self.extra_themes[i])
        if (icon != None):
          break
      else:
        icon = IconTheme.getIconPath(name, size)

      if (icon == None):
        icon = ""

      return icon
