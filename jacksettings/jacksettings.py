#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import dbus, sys
from PyQt4.QtCore import Qt, SIGNAL
from PyQt4.QtGui import QApplication, QDialog, QDialogButtonBox, QMessageBox

# Imports (Plugins and Resources)
import ui_settings_jack

global jackctl
jackctl = None

state_driver_list = ["alsa","firewire","netone"]

# Connect to DBus
def initBus(bus):
  global jackctl
  try:
    jackctl = dbus.Interface(bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller"), "org.jackaudio.Configure")
    return 0
  except:
    jackctl = None
    return 1

def getBufferSize():
  if (jackctl and jackctl.GetParameterValue(["engine","driver"])[2] in state_driver_list):
    return jackctl.GetParameterValue(["driver","period"])[2]
  else:
    return None

def getSampleRate():
  if (jackctl and jackctl.GetParameterValue(["engine","driver"])[2] in state_driver_list):
    return jackctl.GetParameterValue(["driver","rate"])[2]
  else:
    return None

def isRealtime():
  return bool(jackctl.GetParameterValue(["engine","realtime"])[2])

def setBufferSize(bsize):
  bsize = dbus.UInt32(bsize)
  if (jackctl and jackctl.GetParameterValue(["engine","driver"])[2] in state_driver_list):
    return jackctl.SetParameterValue(["driver","period"],bsize)
  else:
    return False

def setSampleRate(srate):
  srate = dbus.UInt32(srate)
  if (jackctl and jackctl.GetParameterValue(["engine","driver"])[2] in state_driver_list):
    return jackctl.SetParameterValue(["driver","rate"],srate)
  else:
    return False

# Jack Settings Dialog
class JackSettingsW(QDialog, ui_settings_jack.Ui_JackSettingsW):
    def __init__(self, parent=None):
        super(JackSettingsW, self).__init__(parent)
        self.setupUi(self)

        if not jackctl:
          QMessageBox.warning(self, self.tr("Failed"), self.tr("jackdbus is not available!\nIs not possible to configure JACK at this point."))

        self.driver = None
        driver_list = jackctl.ReadContainer(["drivers"])[1]

        try:
          jackctl.GetParameterValue(["engine","self-connect-mode"])
        except:
          self.group_server_self_connect_mode.setEnabled(False)

        for i in range(self.obj_server_driver.rowCount()):
          self.obj_server_driver.item(0, i).setTextAlignment(Qt.AlignCenter)
          if (not self.obj_server_driver.item(0, i).text().toLower() in driver_list):
            self.obj_server_driver.hideRow(i)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetJackSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveJackSettings)

        self.connect(self.obj_driver_duplex, SIGNAL("clicked(bool)"), self.checkDuplexSelection)
        self.connect(self.obj_server_driver, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkDriverSelection)

        self.loadServerSettings()
        self.loadDriverSettings(True)

        # Load selected Jack driver
        self.driver = jackctl.GetParameterValue(["engine","driver"])[2]
        for i in range(self.obj_server_driver.rowCount()):
          if (self.obj_server_driver.item(i, 0).text().toLower() == self.driver):
            self.obj_server_driver.setCurrentCell(i, 0)
            break

    def resetJackSettings(self):
        if (self.tabWidget.currentIndex() == 0):
          self.loadServerSettings(True, True)
        elif (self.tabWidget.currentIndex() == 1):
          self.loadDriverSettings(True, True)

    def saveJackSettings(self):
        self.saveServerSettings()
        self.saveDriverSettings()

    def loadJackSettings(self):
        self.loadServerSettings()
        self.loadDriverSettings()

    def saveServerSettings(self):
        server_name = unicode(self.obj_server_name.text())
        if (server_name != jackctl.GetParameterValue(["engine","name"])[2]):
          jackctl.SetParameterValue(["engine","name"],server_name)

        realtime = self.obj_server_realtime.isChecked()
        if (realtime != jackctl.GetParameterValue(["engine","realtime"])[2]):
          jackctl.SetParameterValue(["engine","realtime"],realtime)

        realtime_priority = self.obj_server_realtime_priority.value()
        if (realtime_priority != jackctl.GetParameterValue(["engine","realtime-priority"])[2]):
          jackctl.SetParameterValue(["engine","realtime-priority"],realtime_priority)

        temporary = self.obj_server_temporary.isChecked()
        if (temporary != jackctl.GetParameterValue(["engine","temporary"])[2]):
          jackctl.SetParameterValue(["engine","temporary"],temporary)

        verbose = self.obj_server_verbose.isChecked()
        if (verbose != jackctl.GetParameterValue(["engine","verbose"])[2]):
          jackctl.SetParameterValue(["engine","verbose"],verbose)

        client_timeout = self.obj_server_client_timeout.currentText().toInt()[0]
        if (client_timeout != jackctl.GetParameterValue(["engine","client-timeout"])[2]):
          jackctl.SetParameterValue(["engine","client-timeout"],client_timeout)

        # FIXME - need to know how this works, but it's probably broken
        #if (self.obj_server_clock_source_cycle.isChecked()):
          #clock_source = dbus.Byte("c")
        #elif (self.obj_server_clock_source_hpet.isChecked()):
          #clock_source = dbus.Byte("h")
        #elif (self.obj_server_clock_source_system.isChecked()):
          #clock_source = dbus.Byte("s")
        #if (clock_source != jackctl.GetParameterValue(["engine","clock-source"])[2]):
          #jackctl.SetParameterValue(["engine","clock-source"],clock_source)

        port_max = self.obj_server_port_max.currentText().toInt()[0]
        if (port_max != jackctl.GetParameterValue(["engine","port-max"])[2]):
          jackctl.SetParameterValue(["engine","port-max"],port_max)

        replace_registry = self.obj_server_replace_registry.isChecked()
        if (replace_registry != jackctl.GetParameterValue(["engine","replace-registry"])[2]):
          jackctl.SetParameterValue(["engine","replace-registry"],replace_registry)

        sync = self.obj_server_sync.isChecked()
        if (sync != jackctl.GetParameterValue(["engine","sync"])[2]):
          jackctl.SetParameterValue(["engine","sync"],sync)

        if (self.obj_server_self_connect_mode_0.isChecked()):
          self_connect_mode = dbus.Byte(" ")
        elif (self.obj_server_self_connect_mode_1.isChecked()):
          self_connect_mode = dbus.Byte("E")
        elif (self.obj_server_self_connect_mode_2.isChecked()):
          self_connect_mode = dbus.Byte("e")
        elif (self.obj_server_self_connect_mode_3.isChecked()):
          self_connect_mode = dbus.Byte("A")
        elif (self.obj_server_self_connect_mode_4.isChecked()):
          self_connect_mode = dbus.Byte("a")
        if (self_connect_mode != jackctl.GetParameterValue(["engine","self-connect-mode"])[2]):
          jackctl.SetParameterValue(["engine","self-connect-mode"],self_connect_mode)

    def loadServerSettings(self, reset=False, force_reset=False):
        settings = jackctl.ReadContainer(["engine"])
        for i in range(len(settings[1])):
          attribute = settings[1][i]
          if (reset):
            value = jackctl.GetParameterValue(["engine",attribute])[1]
            if (force_reset and attribute != "driver"):
              jackctl.ResetParameterValue(["engine",attribute])
          else:
            value = jackctl.GetParameterValue(["engine",attribute])[2]

          if (attribute == "name"):
            self.obj_server_name.setText(unicode(value))
          elif (attribute == "realtime"):
            self.obj_server_realtime.setChecked(bool(value))
          elif (attribute == "realtime-priority"):
            self.obj_server_realtime_priority.setValue(int(value))
          elif (attribute == "temporary"):
            self.obj_server_temporary.setChecked(bool(value))
          elif (attribute == "verbose"):
            self.obj_server_verbose.setChecked(bool(value))
          elif (attribute == "client-timeout"):
            self.setComboBoxValue(self.obj_server_client_timeout, str(value))
          elif (attribute == "clock-source"):
            value = str(value)
            if (value == "h"):
              self.obj_server_clock_source_hpet.setChecked(True)
            elif (value == "s"):
              self.obj_server_clock_source_system.setChecked(True)
            elif (value == "c"):
              self.obj_server_clock_source_cycle.setChecked(True)
          elif (attribute == "port-max"):
            self.setComboBoxValue(self.obj_server_port_max, str(value))
          elif (attribute == "replace-registry"):
            self.obj_server_replace_registry.setChecked(bool(value))
          elif (attribute == "sync"):
            self.obj_server_sync.setChecked(bool(value))
          elif (attribute == "self-connect-mode"):
            value = str(value)
            if (value == "E"):
              self.obj_server_self_connect_mode_1.setChecked(True)
            elif (value == "e"):
              self.obj_server_self_connect_mode_2.setChecked(True)
            elif (value == "A"):
              self.obj_server_self_connect_mode_3.setChecked(True)
            elif (value == "a"):
              self.obj_server_self_connect_mode_4.setChecked(True)
            else: #(value == " ")
              self.obj_server_self_connect_mode_0.setChecked(True)

    def saveDriverSettings(self):
        if (self.obj_driver_device.isEnabled()):
          value = unicode(self.obj_driver_device.currentText()).split(" ")[0]
          if (value != jackctl.GetParameterValue(["driver","device"])[2]):
            jackctl.SetParameterValue(["driver","device"],value)

        if (self.obj_driver_capture.isEnabled()):
          if (self.driver == "alsa"):
            value = unicode(self.obj_driver_capture.currentText()).split(" ")[0]
          elif (self.driver == "firewire"):
            value = True if (self.obj_driver_capture.currentIndex() == 1) else False
          if (value != jackctl.GetParameterValue(["driver","capture"])[2]):
            jackctl.SetParameterValue(["driver","capture"],value)

        if (self.obj_driver_playback.isEnabled()):
          if (self.driver == "alsa"):
            value = unicode(self.obj_driver_playback.currentText()).split(" ")[0]
          elif (self.driver == "firewire"):
            value = True if (self.obj_driver_playback.currentIndex() == 1) else False
          if (value != jackctl.GetParameterValue(["driver","playback"])[2]):
            jackctl.SetParameterValue(["driver","playback"],value)

        if (self.obj_driver_rate.isEnabled()):
          value = dbus.UInt32(self.obj_driver_rate.currentText().toInt()[0])
          if (value != jackctl.GetParameterValue(["driver","rate"])[2]):
            jackctl.SetParameterValue(["driver","rate"],value)

        if (self.obj_driver_period.isEnabled()):
          value = dbus.UInt32(self.obj_driver_period.currentText().toInt()[0])
          if (value != jackctl.GetParameterValue(["driver","period"])[2]):
            jackctl.SetParameterValue(["driver","period"],value)

        if (self.obj_driver_nperiods.isEnabled()):
          value = dbus.UInt32(self.obj_driver_nperiods.value())
          if (value != jackctl.GetParameterValue(["driver","nperiods"])[2]):
            jackctl.SetParameterValue(["driver","nperiods"],value)

        if (self.obj_driver_hwmon.isEnabled()):
          value = self.obj_driver_hwmon.isChecked()
          if (value != jackctl.GetParameterValue(["driver","hwmon"])[2]):
            jackctl.SetParameterValue(["driver","hwmon"],value)

        if (self.obj_driver_hwmeter.isEnabled()):
          value = self.obj_driver_hwmeter.isChecked()
          if (value != jackctl.GetParameterValue(["driver","hwmeter"])[2]):
            jackctl.SetParameterValue(["driver","hwmeter"],value)

        if (self.obj_driver_duplex.isEnabled()):
          value = self.obj_driver_duplex.isChecked()
          if (value != jackctl.GetParameterValue(["driver","duplex"])[2]):
            jackctl.SetParameterValue(["driver","duplex"],value)

        if (self.obj_driver_softmode.isEnabled()):
          value = self.obj_driver_softmode.isChecked()
          if (value != jackctl.GetParameterValue(["driver","softmode"])[2]):
            jackctl.SetParameterValue(["driver","softmode"],value)

        if (self.obj_driver_monitor.isEnabled()):
          value = self.obj_driver_monitor.isChecked()
          if (value != jackctl.GetParameterValue(["driver","monitor"])[2]):
            jackctl.SetParameterValue(["driver","monitor"],value)

        if (self.obj_driver_dither.isEnabled()):
          if (self.obj_driver_dither.currentIndex() == 1):
            value = dbus.Byte("r")
          elif (self.obj_driver_dither.currentIndex() == 2):
            value = dbus.Byte("s")
          elif (self.obj_driver_dither.currentIndex() == 3):
            value = dbus.Byte("t")
          else: # 0
            value = dbus.Byte("n")
          if (value != jackctl.GetParameterValue(["driver","dither"])[2]):
            jackctl.SetParameterValue(["driver","dither"],value)

        if (self.obj_driver_inchannels.isEnabled()):
          value = dbus.UInt32(self.obj_driver_inchannels.value())
          if (value != jackctl.GetParameterValue(["driver","inchannels"])[2]):
            jackctl.SetParameterValue(["driver","inchannels"],value)

        if (self.obj_driver_outchannels.isEnabled()):
          value = dbus.UInt32(self.obj_driver_outchannels.value())
          if (value != jackctl.GetParameterValue(["driver","outchannels"])[2]):
            jackctl.SetParameterValue(["driver","outchannels"],value)

        if (self.obj_driver_shorts.isEnabled()):
          value = self.obj_driver_shorts.isChecked()
          if (value != jackctl.GetParameterValue(["driver","shorts"])[2]):
            jackctl.SetParameterValue(["driver","shorts"],value)

        if (self.obj_driver_input_latency.isEnabled()):
          value = dbus.UInt32(self.obj_driver_input_latency.value())
          if (value != jackctl.GetParameterValue(["driver","input-latency"])[2]):
            jackctl.SetParameterValue(["driver","input-latency"],value)

        if (self.obj_driver_output_latency.isEnabled()):
          value = dbus.UInt32(self.obj_driver_output_latency.value())
          if (value != jackctl.GetParameterValue(["driver","output-latency"])[2]):
            jackctl.SetParameterValue(["driver","output-latency"],value)

        if (self.obj_driver_midi_driver.isEnabled()):
          if (self.obj_driver_midi_driver.currentIndex() == 1):
            value = "seq"
          elif (self.obj_driver_midi_driver.currentIndex() == 2):
            value = "raw"
          else: # 0
            value = "none"
          if (value != jackctl.GetParameterValue(["driver","midi-driver"])[2]):
            jackctl.SetParameterValue(["driver","midi-driver"],value)

        if (self.obj_driver_wait.isEnabled()):
          value = dbus.UInt32(self.obj_driver_wait.value())
          if (value != jackctl.GetParameterValue(["driver","wait"])[2]):
            jackctl.SetParameterValue(["driver","wait"],value)

        if (self.obj_driver_verbose.isEnabled()):
          value = dbus.UInt32(self.obj_driver_verbose.value())
          if (value != jackctl.GetParameterValue(["driver","verbose"])[2]):
            jackctl.SetParameterValue(["driver","verbose"],value)

        if (self.obj_driver_snoop.isEnabled()):
          value = self.obj_driver_snoop.isChecked()
          if (value != jackctl.GetParameterValue(["driver","snoop"])[2]):
            jackctl.SetParameterValue(["driver","snoop"],value)

        if (self.obj_driver_channels.isEnabled()):
          value = self.obj_driver_channels.value()
          if (value != jackctl.GetParameterValue(["driver","channels"])[2]):
            jackctl.SetParameterValue(["driver","channels"],value)

    def loadDriverSettings(self, reset=False, force_reset=False):
        settings = jackctl.ReadContainer(["driver"])
        for i in range(len(settings[1])):
          attribute = settings[1][i]
          if (reset):
            value = jackctl.GetParameterValue(["driver",attribute])[1]
            if (force_reset):
              jackctl.ResetParameterValue(["driver",attribute])
          else:
            value = jackctl.GetParameterValue(["driver",attribute])[2]

          if (attribute == "device"):
            self.setComboBoxValue(self.obj_driver_device, str(value), True)
          elif (attribute == "capture"):
            if (self.driver == "alsa"):
              self.setComboBoxValue(self.obj_driver_capture, str(value), True)
            elif (self.driver == "firewire"):
              if (bool(value)):
                self.obj_driver_capture.setCurrentIndex(1)
              else:
                self.obj_driver_capture.setCurrentIndex(0)
          elif (attribute == "playback"):
            if (self.driver == "alsa"):
              self.setComboBoxValue(self.obj_driver_playback, str(value), True)
            elif (self.driver == "firewire"):
              if (bool(value)):
                self.obj_driver_playback.setCurrentIndex(1)
              else:
                self.obj_driver_playback.setCurrentIndex(0)
          elif (attribute == "rate"):
            self.setComboBoxValue(self.obj_driver_rate, str(value))
          elif (attribute == "period"):
            self.setComboBoxValue(self.obj_driver_period, str(value))
          elif (attribute == "nperiods"):
            self.obj_driver_nperiods.setValue(int(value))
          elif (attribute == "hwmon"):
            self.obj_driver_hwmon.setChecked(bool(value))
          elif (attribute == "hwmeter"):
            self.obj_driver_hwmeter.setChecked(bool(value))
          elif (attribute == "duplex"):
            self.obj_driver_duplex.setChecked(bool(value))
          elif (attribute == "softmode"):
            self.obj_driver_softmode.setChecked(bool(value))
          elif (attribute == "monitor"):
            self.obj_driver_monitor.setChecked(bool(value))
          elif (attribute == "dither"):
            value = str(value)
            if (value == "n"):
              self.obj_driver_dither.setCurrentIndex(0)
            elif (value == "r"):
              self.obj_driver_dither.setCurrentIndex(1)
            elif (value == "s"):
              self.obj_driver_dither.setCurrentIndex(2)
            elif (value == "t"):
              self.obj_driver_dither.setCurrentIndex(3)
          elif (attribute == "inchannels"):
            self.obj_driver_inchannels.setValue(int(value))
          elif (attribute == "outchannels"):
            self.obj_driver_outchannels.setValue(int(value))
          elif (attribute == "shorts"):
            self.obj_driver_shorts.setChecked(bool(value))
          elif (attribute == "input-latency"):
            self.obj_driver_input_latency.setValue(int(value))
          elif (attribute == "output-latency"):
            self.obj_driver_output_latency.setValue(int(value))
          elif (attribute == "midi-driver"):
            value = str(value)
            if (value == "none"):
              self.obj_driver_midi_driver.setCurrentIndex(0)
            elif (value == "seq"):
              self.obj_driver_midi_driver.setCurrentIndex(1)
            elif (value == "raw"):
              self.obj_driver_midi_driver.setCurrentIndex(2)
          elif (attribute == "wait"):
            self.obj_driver_wait.setValue(int(value))
          elif (attribute == "verbose"):
            self.obj_driver_verbose.setValue(int(value))
          elif (attribute == "snoop"):
            self.obj_driver_snoop.setChecked(bool(value))
          elif (attribute == "channels"):
            self.obj_driver_channels.setValue(int(value))

    def setComboBoxValue(self, box, text, split=False):
        for i in range(box.count()):
          if (box.itemText(i) == text or (box.itemText(i).split(" ")[0] == text and split)):
            box.setCurrentIndex(i)
            break
        else:
          if (text):
            box.addItem(text)
            box.setCurrentIndex(box.count()-1)

    def checkDuplexSelection(self, active):
        if (self.driver == "alsa" or self.driver == "firewire"):
          self.obj_driver_capture.setEnabled(active)
          self.obj_driver_capture_label.setEnabled(active)
          self.obj_driver_inchannels.setEnabled(active)
          self.obj_driver_inchannels_label.setEnabled(active)
          self.obj_driver_input_latency.setEnabled(active)
          self.obj_driver_input_latency_label.setEnabled(active)

    def checkDriverSelection(self, row, column, prev_row, prev_column):
        # Save previous settings
        self.saveDriverSettings()

        # Set new Jack driver
        self.driver = unicode(self.obj_server_driver.item(row, 0).text().toLower())
        jackctl.SetParameterValue(["engine","driver"],self.driver)

        # Hack for device
        self.obj_driver_device.clear()
        if (self.driver == "alsa" or self.driver == "firewire"):
          dev_list = jackctl.GetParameterConstraint(["driver","device"])[3]
          for i in range(len(dev_list)):
            self.obj_driver_device.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")

        # Hack for alsa and firewire capture/playback
        self.obj_driver_capture.clear()
        self.obj_driver_playback.clear()
        if (self.driver == "alsa"):
          self.obj_driver_capture.addItem("none")
          self.obj_driver_playback.addItem("none")
          dev_list = jackctl.GetParameterConstraint(["driver","device"])[3]
          for i in range(len(dev_list)):
            self.obj_driver_capture.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")
            self.obj_driver_playback.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")
        elif (self.driver == "firewire"):
          self.obj_driver_capture.addItem("no")
          self.obj_driver_capture.addItem("yes")
          self.obj_driver_playback.addItem("no")
          self.obj_driver_playback.addItem("yes")

        # Load Driver Settings
        self.loadDriverSettings()

        # Disable all widgets
        self.obj_driver_capture.setEnabled(False)
        self.obj_driver_capture_label.setEnabled(False)
        self.obj_driver_playback.setEnabled(False)
        self.obj_driver_playback_label.setEnabled(False)
        self.obj_driver_device.setEnabled(False)
        self.obj_driver_device_label.setEnabled(False)
        self.obj_driver_rate.setEnabled(False)
        self.obj_driver_rate_label.setEnabled(False)
        self.obj_driver_period.setEnabled(False)
        self.obj_driver_period_label.setEnabled(False)
        self.obj_driver_nperiods.setEnabled(False)
        self.obj_driver_nperiods_label.setEnabled(False)
        self.obj_driver_hwmon.setEnabled(False)
        self.obj_driver_hwmeter.setEnabled(False)
        self.obj_driver_duplex.setEnabled(False)
        self.obj_driver_softmode.setEnabled(False)
        self.obj_driver_monitor.setEnabled(False)
        self.obj_driver_dither.setEnabled(False)
        self.obj_driver_dither_label.setEnabled(False)
        self.obj_driver_inchannels.setEnabled(False)
        self.obj_driver_inchannels_label.setEnabled(False)
        self.obj_driver_outchannels.setEnabled(False)
        self.obj_driver_outchannels_label.setEnabled(False)
        self.obj_driver_shorts.setEnabled(False)
        self.obj_driver_input_latency.setEnabled(False)
        self.obj_driver_input_latency_label.setEnabled(False)
        self.obj_driver_output_latency.setEnabled(False)
        self.obj_driver_output_latency_label.setEnabled(False)
        self.obj_driver_midi_driver.setEnabled(False)
        self.obj_driver_midi_driver_label.setEnabled(False)
        self.obj_driver_wait.setEnabled(False)
        self.obj_driver_wait_label.setEnabled(False)
        self.obj_driver_verbose.setEnabled(False)
        self.obj_driver_verbose_label.setEnabled(False)
        self.obj_driver_snoop.setEnabled(False)
        self.obj_driver_channels.setEnabled(False)
        self.obj_driver_channels_label.setEnabled(False)

        # Restore widgets according to driver
        if (self.obj_server_driver.item(0, row).text() == "ALSA"):
          self.toolbox_driver_misc.setCurrentIndex(1)
          self.obj_driver_capture.setEnabled(True)
          self.obj_driver_capture_label.setEnabled(True)
          self.obj_driver_capture_label.setText(self.tr("Input Device:"))
          self.obj_driver_playback.setEnabled(True)
          self.obj_driver_playback_label.setEnabled(True)
          self.obj_driver_playback_label.setText(self.tr("Output Device:"))
          self.obj_driver_device.setEnabled(True)
          self.obj_driver_device_label.setEnabled(True)
          self.obj_driver_rate.setEnabled(True)
          self.obj_driver_rate_label.setEnabled(True)
          self.obj_driver_period.setEnabled(True)
          self.obj_driver_period_label.setEnabled(True)
          self.obj_driver_nperiods.setEnabled(True)
          self.obj_driver_nperiods_label.setEnabled(True)
          self.obj_driver_hwmon.setEnabled(True)
          self.obj_driver_hwmeter.setEnabled(True)
          self.obj_driver_duplex.setEnabled(True)
          self.obj_driver_softmode.setEnabled(True)
          self.obj_driver_monitor.setEnabled(True)
          self.obj_driver_dither.setEnabled(True)
          self.obj_driver_dither_label.setEnabled(True)
          self.obj_driver_inchannels.setEnabled(True)
          self.obj_driver_inchannels_label.setEnabled(True)
          self.obj_driver_outchannels.setEnabled(True)
          self.obj_driver_outchannels_label.setEnabled(True)
          self.obj_driver_shorts.setEnabled(True)
          self.obj_driver_input_latency.setEnabled(True)
          self.obj_driver_input_latency_label.setEnabled(True)
          self.obj_driver_output_latency.setEnabled(True)
          self.obj_driver_output_latency_label.setEnabled(True)
          self.obj_driver_midi_driver.setEnabled(True)
          self.obj_driver_midi_driver_label.setEnabled(True)

        elif (self.obj_server_driver.item(0, row).text() == "Dummy"):
          self.toolbox_driver_misc.setCurrentIndex(2)
          self.obj_driver_wait.setEnabled(True)
          self.obj_driver_wait_label.setEnabled(True)

        elif (self.obj_server_driver.item(0, row).text() == "FireWire"):
          self.toolbox_driver_misc.setCurrentIndex(3)
          self.obj_driver_capture.setEnabled(True)
          self.obj_driver_capture_label.setEnabled(True)
          self.obj_driver_capture_label.setText(self.tr("Capture Ports:"))
          self.obj_driver_playback.setEnabled(True)
          self.obj_driver_playback_label.setEnabled(True)
          self.obj_driver_playback_label.setText(self.tr("Playback Ports:"))
          self.obj_driver_device.setEnabled(True)
          self.obj_driver_device_label.setEnabled(True)
          self.obj_driver_rate.setEnabled(True)
          self.obj_driver_rate_label.setEnabled(True)
          self.obj_driver_period.setEnabled(True)
          self.obj_driver_period_label.setEnabled(True)
          self.obj_driver_nperiods.setEnabled(True)
          self.obj_driver_nperiods_label.setEnabled(True)
          self.obj_driver_duplex.setEnabled(True)
          self.obj_driver_inchannels.setEnabled(True)
          self.obj_driver_inchannels_label.setEnabled(True)
          self.obj_driver_outchannels.setEnabled(True)
          self.obj_driver_outchannels_label.setEnabled(True)
          self.obj_driver_input_latency.setEnabled(True)
          self.obj_driver_input_latency_label.setEnabled(True)
          self.obj_driver_output_latency.setEnabled(True)
          self.obj_driver_output_latency_label.setEnabled(True)
          self.obj_driver_verbose.setEnabled(True)
          self.obj_driver_verbose_label.setEnabled(True)
          self.obj_driver_snoop.setEnabled(True)

        elif (self.obj_server_driver.item(0, row).text() == "Loopback"):
          self.toolbox_driver_misc.setCurrentIndex(4)
          self.obj_driver_channels.setEnabled(True)
          self.obj_driver_channels_label.setEnabled(True)

        elif (self.obj_server_driver.item(0, row).text() == "Net"):
          self.toolbox_driver_misc.setCurrentIndex(0)

        elif (self.obj_server_driver.item(0, row).text() == "NetOne"):
          self.toolbox_driver_misc.setCurrentIndex(0)
          self.obj_driver_rate.setEnabled(True)
          self.obj_driver_rate_label.setEnabled(True)
          self.obj_driver_period.setEnabled(True)
          self.obj_driver_period_label.setEnabled(True)

        self.checkDuplexSelection(self.obj_driver_duplex.isChecked())



# Allow to use this as a standalone app
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)

    # Connect to DBus
    initBus(dbus.SessionBus())

    # Show GUI
    gui = JackSettingsW()
    gui.show()

    # App-Loop
    sys.exit(app.exec_())
